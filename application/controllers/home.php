<?php

class Home_Controller extends Base_Controller {

	/*
	|--------------------------------------------------------------------------
	| The Default Controller
	|--------------------------------------------------------------------------
	|
	| Instead of using RESTful routes and anonymous functions, you might wish
	| to use controllers to organize your application API. You'll love them.
	|
	| This controller responds to URIs beginning with "home", and it also
	| serves as the default controller for the application, meaning it
	| handles requests to the root of the application.
	|
	| You can respond to GET requests to "/home/profile" like so:
	|
	|		public function action_profile()
	|		{
	|			return "This is your profile!";
	|		}
	|
	| Any extra segments are passed to the method as parameters:
	|
	|		public function action_profile($id)
	|		{
	|			return "This is the profile for user {$id}.";
	|		}
	|
	*/

	public function action_index()
	{
		return View::make('home.index');
	}
		public function post_upload()
	{
		$input = Input::all();
		$user = Auth::user();
		$image = Input::file('headshot');
		$layer = PHPImageWorkshop\ImageWorkshop::initFromPath($image['tmp_name']);

		$dirPath = path('storage').'/uploads/thumbnails/avatars/';
		$filename = Auth::user()->id. uniqid($image['size']) .'.jpg';
		$createFolders = true;
		$backgroundColor = null; // transparent, only for PNG (otherwise it will be white if set null)
		$imageQuality = 100; // useless for GIF, usefull for PNG and JPEG (0 to 100%)
		$layer->resizeInPixel(377, 310, true, 0, 0, 'MM');
		$layer->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);

		$user->headshot = $filename;
		$user->headshot_file = $image['name'];
		$user->save();

		return Redirect::to('/');

	}
	
	public function get_upload(){
		echo 'We come in peace';
	}

}